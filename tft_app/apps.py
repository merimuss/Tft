from django.apps import AppConfig


class TftAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tft_app'
