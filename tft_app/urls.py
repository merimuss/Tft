from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from tft_app.views import *

urlpatterns = [
    path('registration/', Registration.as_view(), name='basic-registration'),

    path('auth/token/', TokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('auth/token/refresh/', TokenRefreshView.as_view(), name='token-refresh'),

    path('study-programs-and-classes/list/', FacultyAndClasses.as_view(), name='list-study-programs-and-classes'),

    # ADMINISTRATOR
    path('admin/admins/add-new/', RegisterNewAdmin.as_view(), name='add-new-admin'),
    path('admin/admins/list/', ListAdmins.as_view(), name='list-admins'),
    path('admin/admins/del/<int:user_id>', DeleteAdmin.as_view(), name='delete-admin'),
    # Applications
    path('admin/applications/list/', AllApplications.as_view(), name=''),
    path('admin/applications/accept/<int:application_id>', AcceptApplication.as_view(), name=''),
    path('admin/enrolled/list/', StudentsEnrolledPerProgram.as_view(), name=''),

    # STUDENT
    # Application form initial data
    path('student/initial-data/add/', StudentDataPost.as_view(), name=''),
    path('student/initial-data/edit/<int:student_data_id>', StudentDataPatch.as_view(), name=''),
    path('student/initial-data/view/', StudentDataGet.as_view(), name=''),
    # Application form for a specific study program
    path('student/application/add/', SubmitApplication.as_view(), name=''),
    path('student/application/view/', StudentSubmittedApplications.as_view(), name=''),
    path('student/application/del/<int:application_id>', DeleteSubmittedApplications.as_view(), name=''),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

