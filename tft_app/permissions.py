from rest_framework.permissions import BasePermission


class AdminPermission(BasePermission):

    def has_permission(self, request, view):
        return request.user.role == 'Administrator'

    def has_object_permission(self, request, view, obj):
        return request.user.role == 'Administrator'


class StudentPermission(BasePermission):

    def has_permission(self, request, view):
        return request.user.role == 'Student'

    def has_object_permission(self, request, view, obj):
        return request.user.role == 'Student'
