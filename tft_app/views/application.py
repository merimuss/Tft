import os

from django.conf import settings

from rest_framework import status
from rest_framework.exceptions import ValidationError
from rest_framework.generics import GenericAPIView, get_object_or_404, ListAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from tft_app.permissions import StudentPermission, AdminPermission
from tft_app.serializers import *


# ------------------------------------------------- student ------------------------------------------------------------
def enrolled(student_user_id):
    try:
        StudentsEnrolledInProgram.objects.get(student_id=student_user_id)
        return True
    except StudentsEnrolledInProgram.DoesNotExist:
        return False


class StudentDataPost(GenericAPIView):
    """ Application form initial data needed by student
    """
    permission_classes = [IsAuthenticated, StudentPermission]
    serializer_class = StudentDataSerializer
    parser_classes = [MultiPartParser, ]

    def post(self, request):

        if enrolled(request.user.id):
            raise ValidationError('Student already accepted and enrolled to another study program')

        if len(request.data['school_certificate']) == 0:
            raise ValidationError('File must be provided')
        serializer = self.serializer_class(data=request.data, context={'request': self.request.user.pk})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)


class StudentDataGet(GenericAPIView):
    """ student == user_id
    """
    permission_classes = [IsAuthenticated, StudentPermission]
    serializer_class = StudentDataGetSerializer

    def get(self, request):
        try:
            query = StudentData.objects.get(student_id=request.user.id)
            serializer = self.serializer_class(query)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except StudentData.DoesNotExist:
            return Response('No data', status=status.HTTP_204_NO_CONTENT)


class StudentDataPatch(GenericAPIView):
    permission_classes = [IsAuthenticated, StudentPermission]
    serializer_class = StudentDataPatchSerializer
    parser_classes = [MultiPartParser, ]

    def patch(self, request, student_data_id):

        if enrolled(request.user.id):
            raise ValidationError('Student already accepted and enrolled to another study program')

        if len(request.data['school_certificate']) == 0:
            raise ValidationError('File must be provided')

        query = get_object_or_404(StudentData, id=student_data_id)
        serializer = self.serializer_class(
            instance=query,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        new_name = str(request.user.id) + '-sc-' + str(request.user.last_name) + '.' + \
                                        query.school_certificate.name.split('.')[1]

        os.rename(os.path.join(settings.MEDIA_ROOT, query.school_certificate.name),
                  os.path.join(settings.MEDIA_ROOT, new_name))

        return Response(status=status.HTTP_200_OK)


class SubmitApplication(GenericAPIView):
    """
    In order to submit application, student must provide initial data first
    Submit application for a specific study program, students can apply for more than 1
    study_program field: 1 = Technology, 2 = Informatics, 3 = Mathematics
    """
    permission_classes = [IsAuthenticated, StudentPermission]
    serializer_class = SubmitApplicationSerializer
    parser_classes = [MultiPartParser, ]

    def post(self, request):

        if enrolled(request.user.id):
            raise ValidationError('Student already accepted and enrolled to another study program')

        try:
            sd = StudentData.objects.get(student_id=request.user.id)
        except StudentData.DoesNotExist:
            raise ValidationError('Initial data must be provided')

        try:
            Application.objects.get(student_data=sd, study_program=request.data['study_program'])
            raise ValidationError('Application already submitted for this study program')
        except Application.DoesNotExist:
            pass

        if len(request.data['application_letter']) == 0:
            raise ValidationError('Application letter must be provided')

        serializer = self.serializer_class(data=request.data, context={'request': self.request.user.pk})

        count_enrolled = StudentsEnrolledInProgram.objects.filter(study_program=request.data['study_program']).count()
        quota = get_object_or_404(Faculty, id=request.data['study_program']).quota

        if count_enrolled < quota:
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response('Application successfully submitted', status=status.HTTP_201_CREATED)
        return Response('Application cannot be submitted, quota fulfilled', status=status.HTTP_400_BAD_REQUEST)


class StudentSubmittedApplications(GenericAPIView):
    """Student's submitted applications with statuses
    id == application id
    """
    permission_classes = [IsAuthenticated, StudentPermission]
    serializer_class = AllStudentsApplicationsSerializer

    def get(self, request):
        query = Application.objects.filter(student_data__student=request.user.id)
        serializer = self.serializer_class(query, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class DeleteSubmittedApplications(GenericAPIView):
    """Delete submitted application - possible only if no application was accepted so far
    """
    permission_classes = [IsAuthenticated, StudentPermission]

    def delete(self, request, application_id):

        if enrolled(request.user.id):
            raise ValidationError('Student already accepted and enrolled to another study program')

        try:
            appl = Application.objects.get(id=application_id, student_data__student_id=request.user.id)
            if ApplicationStatus.objects.filter(application=appl, status='Accepted').count() == 0:
                appl.delete()
                ApplicationStatus.objects.filter(application=appl).delete()
            return Response('Deleted', status=status.HTTP_204_NO_CONTENT)
        except Application.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)


# ---------------------------------------------- administrator ---------------------------------------------------------
class AllApplications(ListAPIView):
    """All students' applications, with all statuses
    study_program field: 1 = Technology, 2 = Informatics, 3 = Mathematics
    id (the variable on top) == application_id
    """
    permission_classes = [IsAuthenticated, AdminPermission]
    serializer_class = AllApplicationsSerializer
    queryset = Application.objects.all()


class AcceptApplication(GenericAPIView):
    """Set application status to "Accepted"
    'approval_reason' field is required
    """
    permission_classes = [IsAuthenticated, AdminPermission]
    serializer_class = AcceptApplicationSerializer

    def post(self, request, application_id):

        student_id = StudentData.objects.get(application=application_id).student_id
        if enrolled(student_id):
            raise ValidationError('Student already accepted and enrolled to another study program')

        if len(request.data['approval_reason']) == 0:
            raise ValidationError('This entry is required')

        try:
            ApplicationStatus.objects.get(application_id=application_id)
            serializer = self.serializer_class(data=request.data,
                                               context={
                                                   'admin_id': self.request.user.pk,
                                                   'application_id': application_id
                                               })
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response('Application accepted!', status=status.HTTP_200_OK)
        except ApplicationStatus.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class StudentsEnrolledPerProgram(ListAPIView):
    """List of all enrolled students for each program
    """
    permission_classes = [IsAuthenticated, AdminPermission]
    serializer_class = StudentsEnrolledSerializer
    queryset = StudentsEnrolledInProgram.objects.all().order_by('study_program')
