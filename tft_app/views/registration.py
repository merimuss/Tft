from rest_framework import status
from rest_framework.generics import GenericAPIView, get_object_or_404, ListAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response

from tft_app.permissions import AdminPermission
from tft_app.serializers import *


class Registration(GenericAPIView):

    permission_classes = [AllowAny, ]
    serializer_class = RegistrationSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if not request.user.is_authenticated:
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response('Registration successful', status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class RegisterNewAdmin(GenericAPIView):

    permission_classes = [IsAuthenticated, AdminPermission]
    serializer_class = RegisterNewAdminSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response('New administrator successfully registered', status=status.HTTP_201_CREATED)


class ListAdmins(ListAPIView):

    permission_classes = [IsAuthenticated, AdminPermission]
    serializer_class = ListAdminsSerializer
    queryset = User.objects.filter(role='Administrator')


class DeleteAdmin(GenericAPIView):

    permission_classes = [IsAuthenticated, AdminPermission]

    def delete(self, request, user_id):
        user = get_object_or_404(User, id=user_id)
        if user.role == 'Administrator' and self.request.user.pk != user_id:
            user.delete()
            return Response('Administrator deleted', status=status.HTTP_204_NO_CONTENT)
        else:
            # Administrator cannot delete itself
            return Response(status=status.HTTP_400_BAD_REQUEST)
