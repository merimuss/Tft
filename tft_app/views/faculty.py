from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated

from tft_app.models import Faculty
from tft_app.serializers import FacultyAndClassesSerializer


class FacultyAndClasses(ListAPIView):
    """List of all study programs with associated classes and information related to them
    """
    permission_classes = [IsAuthenticated]
    serializer_class = FacultyAndClassesSerializer
    queryset = Faculty.objects.all()
