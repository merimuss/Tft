from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models


class UserManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, first_name, last_name, role, email, password=None):
        user = self.model(
            first_name=first_name,
            last_name=last_name,
            role=role,
            email=self.normalize_email(email)
        )
        user.set_password(password)
        user.is_staff = False
        user.is_superuser = False
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name='', last_name='', password=None, role=None):
        user = self.create_user(first_name, last_name, role, email)
        user.role = None
        user.set_password(password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    USER_ROLE = [
        ('Student', 'Student'),
        ('Administrator', 'Administrator')
    ]
    username = None
    first_name = models.CharField(max_length=108, blank=True)
    last_name = models.CharField(max_length=108, blank=True)
    email = models.EmailField(max_length=108, unique=True)
    password = models.CharField(max_length=108)

    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    role = models.CharField(choices=USER_ROLE, default=None, max_length=13, null=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    def full_name(self):
        return self.first_name + ' ' + self.last_name
