import os.path

from django.conf import settings
from django.db import models

from tft_app.models import Faculty, User


class StudentData(models.Model):
    student = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='User_id')
    birth_date = models.DateField()
    birth_place = models.CharField(max_length=108)
    high_school = models.CharField(max_length=108)
    school_certificate = models.FileField(upload_to=os.path.join(settings.MEDIA_ROOT))
    # assuming student's average grade is the same for all 3 study programs
    average_grade = models.DecimalField(max_digits=5, decimal_places=2)
    graduation_grade = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Overall graduation grade')


class Application(models.Model):
    student_data = models.ForeignKey(StudentData, on_delete=models.CASCADE)
    application_letter = models.FileField(upload_to=os.path.join(settings.MEDIA_ROOT))
    study_program = models.ForeignKey(Faculty, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('student_data', 'study_program')


class ApplicationStatus(models.Model):
    STATUS = [
        ('Submitted', 'Submitted'),
        ('Accepted', 'Accepted'),
        ('Rejected', 'Rejected')
    ]
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS, default='Submitted', max_length=9)
    by_admin = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    approval_reason = models.TextField(blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
