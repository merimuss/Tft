from django.db import models

from tft_app.models import User


# IMPORTANT: LOADDATA FROM 2 FIXTURES: courses.json, faculty.json
class Faculty(models.Model):
    study_program = models.CharField(max_length=108, verbose_name='Academic program title')
    quota = models.PositiveIntegerField(verbose_name='Number of students accepted in the program')
    student_year = models.CharField(max_length=7, default='2022-23', verbose_name='Format data example: 2022-23')

    def __str__(self):
        return self.study_program


class Courses(models.Model):
    course = models.CharField(max_length=333, verbose_name='Course title')
    course_description = models.TextField()
    ects = models.PositiveIntegerField()
    study_program = models.ForeignKey(Faculty, on_delete=models.CASCADE)


class StudentsEnrolledInProgram(models.Model):
    student = models.OneToOneField(User, on_delete=models.CASCADE)
    study_program = models.ForeignKey(Faculty, on_delete=models.CASCADE)

