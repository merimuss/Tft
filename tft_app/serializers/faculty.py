from rest_framework import serializers

from tft_app.models import Courses, Faculty


class CoursesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Courses
        fields = '__all__'


class FacultySerializer(serializers.ModelSerializer):
    class Meta:
        model = Faculty
        fields = '__all__'


class FacultyAndClassesSerializer(serializers.ModelSerializer):
    courses = serializers.SerializerMethodField()

    def get_courses(self, obj):
        courses = Courses.objects.filter(study_program=obj)
        return CoursesSerializer(courses, many=True).data

    class Meta:
        model = Faculty
        fields = ['id', 'study_program', 'quota', 'courses']
