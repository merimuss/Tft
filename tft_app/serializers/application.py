import datetime

from rest_framework import serializers

from tft_app.models import StudentData, User, Application, ApplicationStatus, StudentsEnrolledInProgram


# ----------------------------------------------- student --------------------------------------------------------------
class StudentDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentData
        fields = ['birth_date', 'birth_place', 'high_school', 'school_certificate', 'average_grade', 'graduation_grade']

    def create(self, validated_data):
        user = User.objects.get(id=self.context['request'])
        school_certificate = validated_data.__getitem__('school_certificate')
        school_certificate._name = str(user.id) + '-sc-' + str(user.last_name) + '.' + \
                                   school_certificate._name.split('.')[-1]
        return StudentData.objects.create(
            student=user,
            birth_date=validated_data['birth_date'],
            birth_place=validated_data['birth_place'],
            high_school=validated_data['high_school'],
            school_certificate=school_certificate,
            average_grade=validated_data['average_grade'],
            graduation_grade=validated_data['graduation_grade']
        )


class StudentDataGetSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentData
        fields = '__all__'


class StudentDataPatchSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentData
        fields = ['birth_date', 'birth_place', 'high_school', 'school_certificate', 'average_grade', 'graduation_grade']

    def update(self, instance, validated_data):
        instance.birth_date = validated_data.get('birth_date', instance.birth_date)
        instance.birth_place = validated_data.get('birth_place', instance.birth_place)
        instance.high_school = validated_data.get('high_school', instance.high_school)
        instance.school_certificate = validated_data.get('school_certificate', instance.school_certificate)
        instance.average_grade = validated_data.get('average_grade', instance.average_grade)
        instance.graduation_grade = validated_data.get('graduation_grade', instance.graduation_grade)
        instance.save()
        return instance


class SubmitApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Application
        fields = ['application_letter', 'study_program']

    def create(self, validated_data):
        user = User.objects.get(id=self.context['request'])
        student_data = StudentData.objects.get(student_id=self.context['request'])
        application_letter = validated_data.__getitem__('application_letter')
        application_letter._name = str(user.id) + '-al-' + str(user.last_name) + '-' + \
                                   str(validated_data['study_program']) + '.' + application_letter._name.split('.')[-1]

        ins = Application.objects.create(
            student_data=student_data,
            application_letter=application_letter,
            study_program=validated_data['study_program']
        )
        ApplicationStatus.objects.create(
            application=ins,
            status='Submitted',
            by_admin=None,
            approval_reason=''
        )
        return ins


class ApplicationStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationStatus
        fields = ['status', 'timestamp']


class AllStudentsApplicationsSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()

    def get_status(self, obj):
        status = ApplicationStatus.objects.filter(application=obj)
        return ApplicationStatusSerializer(status, many=True).data

    class Meta:
        model = Application
        fields = ['id', 'study_program', 'status']


# ---------------------------------------------- administrator ---------------------------------------------------------
class AllApplicationStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationStatus
        fields = '__all__'


class StudentData2Serializer(serializers.ModelSerializer):
    class Meta:
        model = StudentData
        exclude = ['school_certificate']


class StudentData3Serializer(serializers.ModelSerializer):
    class Meta:
        model = StudentData
        fields = ['school_certificate']


class ApplicationStatus2Serializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationStatus
        fields = '__all__'


class AllApplicationsSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    student = serializers.SerializerMethodField()
    application_letter = serializers.SerializerMethodField()
    student_data = serializers.SerializerMethodField()
    school_certificate = serializers.SerializerMethodField()

    def get_status(self, obj):
        status = ApplicationStatus.objects.filter(application=obj)
        return ApplicationStatus2Serializer(status, many=True).data

    def get_student(self, obj):
        sd = Application.objects.get(id=obj.id).student_data_id
        student = StudentData.objects.get(id=sd).student
        return student.first_name + ' ' + student.last_name

    def get_application_letter(self, obj):
        return Application.objects.get(id=obj.id).application_letter.path

    def get_student_data(self, obj):
        sd = Application.objects.get(id=obj.id).student_data_id
        s = StudentData.objects.get(id=sd)
        return StudentData2Serializer(s).data

    def get_school_certificate(self, obj):
        sd = Application.objects.get(id=obj.id).student_data_id
        return StudentData.objects.get(id=sd).school_certificate.path

    class Meta:
        model = Application
        fields = ['id', 'student', 'study_program', 'student_data', 'school_certificate', 'application_letter',
                  'status']


class AcceptApplicationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApplicationStatus
        fields = ['approval_reason']

    def create(self, validated_data):
        ins = ApplicationStatus.objects.create(
            application_id=self.context['application_id'],
            approval_reason=validated_data['approval_reason'],
            status='Accepted',
            by_admin_id=self.context['admin_id'],
            timestamp=datetime.datetime.now()
        )
        ap = Application.objects.get(id=self.context['application_id'])
        student = User.objects.get(studentdata=ap.student_data)
        StudentsEnrolledInProgram.objects.create(
            student=student,
            study_program=ap.study_program
        )
        return ins


class FullNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email']


class StudentsEnrolledSerializer(serializers.ModelSerializer):
    student = serializers.SerializerMethodField()

    def get_student(self, obj):
        user = User.objects.get(id=obj.student.id)
        return FullNameSerializer(user).data

    class Meta:
        model = StudentsEnrolledInProgram
        fields = ['study_program', 'student']

