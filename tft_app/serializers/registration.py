from rest_framework import serializers

from tft_app.models import User


class RegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password', 'role']
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, attrs):
        if attrs['role'] is not None:
            if not (attrs['role'] == 'Student' or attrs['role'] == 'Administrator'):
                raise serializers.ValidationError('Possible roles: Student or Administrator')
            first_name = attrs['first_name']
            last_name = attrs['last_name']
            if not first_name.isalpha():
                raise serializers.ValidationError('Enter valid first name')
            if not last_name.isalpha():
                raise serializers.ValidationError('Enter valid last name')
        return attrs

    def create(self, validated_data):
        user = User.objects.create_user(
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            email=validated_data['email'],
            role=validated_data['role']
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class RegisterNewAdminSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, attrs):
        first_name = attrs['first_name']
        last_name = attrs['last_name']
        if not first_name.isalpha():
            raise serializers.ValidationError('Enter valid first name')
        if not last_name.isalpha():
            raise serializers.ValidationError('Enter valid last name')
        return attrs

    def create(self, validated_data):
        user = User.objects.create_user(
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            email=validated_data['email'],
            role='Administrator'
        )
        user.set_password(validated_data['password'])
        user.save()
        return user


class DeleteAdminSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id']


class ListAdminsSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email']
