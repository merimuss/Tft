from django.contrib import admin
from django.urls import path, include, re_path

from drf_yasg.views import get_schema_view
from drf_yasg import openapi

from rest_framework import permissions
from rest_framework.routers import DefaultRouter


schema_view = get_schema_view(
   openapi.Info(
      title="Tft API",
      default_version='v1',
      description="Tft test APIs",
       ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

API_PREFIX = r'^api/v1/'

router = DefaultRouter()

urlpatterns = [
    re_path(API_PREFIX, include([
        # Swagger http://127.0.0.1:8000/api/v1/swagger/
        re_path(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),

        # V1 API routes
        re_path('', include('tft_app.urls')),
    ])),

    path('admin/', admin.site.urls),

]
